import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';
import { TapGestureHandler, State } from 'react-native-gesture-handler';
import SVG, { Image, Circle, ClipPath } from 'react-native-svg';

const { width, height } = Dimensions.get('window'),
      { Value, event, block, cond, eq, set, Clock, startClock,
      stopClock, debug, timing, clockRunning, interpolate, Extrapolate, concat } = Animated;

function runTiming(clock, value, dest) {
    const state = {
      finished: new Value(0),
      position: new Value(0),
      time: new Value(0),
      frameTime: new Value(0)
    };
  
    const config = {
      duration: 1000,
      toValue: new Value(0),
      easing: Easing.inOut(Easing.ease)
    };
  
    return block([
      cond(clockRunning(clock), 0, [
        set(state.finished, 0),
        set(state.time, 0),
        set(state.position, value),
        set(state.frameTime, 0),
        set(config.toValue, dest),
        startClock(clock)
      ]),
      timing(clock, state, config),
      cond(state.finished, debug('stop clock', stopClock(clock))),
      state.position
    ]);
}

export default class SignInScreen extends Component {
  constructor() {
    super();

    this.fieldsClosed = new Value(1);
    this.buttonOpacity = new Value(1);

    this.onStateChange = event([
        {
            nativeEvent: ({ state }) => block([
                cond(eq(state, State.END), set( this.fieldsClosed, runTiming( new Clock(), 1, 0 ) ))
            ])
        }
    ])

    this.onCloseState = event([
        {
            nativeEvent: ({ state }) => block([
                cond(eq(state, State.END), set( this.fieldsClosed, runTiming( new Clock(), 0, 1 ) ))
            ])
        }
    ])
    
    this.buttonOpacity = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [0, 1],
        extrapolate: Extrapolate.CLAMP
    })

    this.buttonY = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [100, 0],
        extrapolate: Extrapolate.CLAMP
    })

    this.bgY = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [-height / 3 - bgOpenedMinusSize, 0],
        extrapolate: Extrapolate.CLAMP
    })

    this.fieldsZIndex = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [1, -1],
        extrapolate: Extrapolate.CLAMP
    })
    this.fieldsOpacity = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [1, 0],
        extrapolate: Extrapolate.CLAMP
    })
    this.fieldsTranslateY = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [1, 100],
        extrapolate: Extrapolate.CLAMP
    })

    this.closeBtnRotate = interpolate( this.fieldsClosed, {
        inputRange: [0, 1],
        outputRange: [180, 360],
        extrapolate: Extrapolate.CLAMP
    })
  }

  goHome = (navigation) => {
    navigation.navigate('Home');
  }

  render() {
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
        <Animated.View style={{ ...styles.absolute, transform: [{ translateY: this.bgY }] }}>
            <SVG height={height+bgMultiSize} width={width}>
                <ClipPath id="clip">
                    <Circle r={height+bgMultiSize} cx={width/2}/>
                </ClipPath>
                <Image
                    href={require('../../assets/bg.jpg')}
                    width={width}
                    height={height+bgMultiSize}
                    preserveAspectRatio="xMidYMid slice"
                    clipPath="url(#clip)"
                />
            </SVG>
        </Animated.View>
        <Animated.View style={{ 
            opacity: this.buttonOpacity,
            transform: [{ translateY: this.buttonY }]
        }}>
            <View style={styles.buttonsContainer}>
                <TapGestureHandler onHandlerStateChange={this.onStateChange}>
                    <Animated.View style={styles.button}>
                        <Text style={styles.buttonText}>Sign in</Text>
                    </Animated.View>
                </TapGestureHandler>
                <TapGestureHandler onHandlerStateChange={this.onStateChange}>
                    <Animated.View style={{ ...styles.button , ...styles.buttonBlue }}>
                        <Text style={{ ...styles.buttonText, ...styles.buttonBlueText }}>Sign in with Facebook</Text>
                    </Animated.View>
                </TapGestureHandler>
            </View>
        </Animated.View>
        <Animated.View style={{ 
            ...styles.fieldsContainer, 
            zIndex: this.fieldsZIndex,  
            opacity: this.fieldsOpacity,
            transform: [{ translateY: this.fieldsTranslateY }]
        }}>
            <TapGestureHandler onHandlerStateChange={this.onCloseState}>
                    <Animated.View style={styles.closeButton}>
                        <Animated.Text style={{ 
                            ...styles.closeButtonText, 
                            transform: [{ rotate: concat(this.closeBtnRotate, 'deg') }] 
                        }}>×</Animated.Text>
                    </Animated.View>
                </TapGestureHandler>
            <TextInput
                placeholder="EMAIL"
                placeholderTextColor="#ccc"
                style={styles.textInput}
            />
            <TextInput
                placeholder="PASSWORD"
                placeholderTextColor="#ccc"
                style={styles.textInput}
            />
            <TapGestureHandler onHandlerStateChange={() => this.goHome(navigation)}>
                <Animated.View style={{ ...styles.button, ...styles.buttonWhiteOnWhite }}>
                    <Text style={styles.buttonText}>Sign in</Text>
                </Animated.View>
            </TapGestureHandler>
        </Animated.View>
      </View>
    );
  }
}


const closeSize = 40,
    bgMultiSize = 100,
    bgOpenedMinusSize = 20,
    styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'flex-end',
    },
    absolute: {
        ...StyleSheet.absoluteFill
    },
    image: {
        flex: 1,
        height: null,
        width: null
    },


    buttonsContainer: { 
        height: height/3,
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'white',
        height: 70,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 8
    },
    buttonBlue: {
        backgroundColor: '#3b5998',
    },
    buttonWhiteOnWhite: {
        borderColor: '#000',
        borderWidth: 2
    },
    buttonText: {
        fontFamily: 'OpenSans-SemiBold',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        fontSize: 20,
        color: '#000'
    },
    buttonBlueText: {
        color: '#fff',
    },


    fieldsContainer: {
        height: height/3,
        ...StyleSheet.absoluteFill,
        top: null,
        justifyContent: 'center',
    },
    textInput: {
        fontFamily: 'OpenSans-Regular',
        height: 50,
        borderRadius: 25,
        borderWidth: 0.5,
        marginHorizontal: 20,
        marginVertical: 10,
        paddingLeft: 25,
        borderColor: 'rgba(0, 0, 0, 0.2)'
    },
    closeButton: {
        height: closeSize,
        width: closeSize,
        backgroundColor: 'white',
        borderRadius: closeSize/2,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: -20,
        left: width/2 - closeSize/2,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    closeButtonText: {
        fontSize: 25,
        transform: [{ translateY: -2 }]
    }
  });