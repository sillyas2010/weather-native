import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import { FontAwesome } from '@expo/vector-icons';
import HomeScreen from './components/HomeScreen/HomeScreen';
import SignInScreen from './components/SignInScreen/SignInScreen';
import { NavigationNativeContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

const config = {
  animation: 'easyinout',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

const Stack = createStackNavigator();

export default class AppContainer extends React.Component {
  state = {
    isReady: false,
  };

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require('./assets/bg.jpg')
    ]);

    const fontAssets = cacheFonts([FontAwesome.font, {
      'OpenSans-Regular': require('./assets/fonts/OpenSans-Regular.ttf'),
      'OpenSans-Bold': require('./assets/fonts/OpenSans-Bold.ttf'),
      'OpenSans-Light': require('./assets/fonts/OpenSans-Light.ttf'),
      'OpenSans-SemiBold': require('./assets/fonts/OpenSans-SemiBold.ttf'),
      'OpenSans-ExtraBold': require('./assets/fonts/OpenSans-ExtraBold.ttf'),
    }]);

    await Promise.all([...imageAssets, ...fontAssets]);
  }

  render() {
      return (<>
        { !this.state.isReady ?
          <AppLoading
            startAsync={this._loadAssetsAsync}
            onFinish={() => this.setState({ isReady: true })}
            onError={console.warn}
          />
          :
          <NavigationNativeContainer>
            <Stack.Navigator
              initialRouteName="Sign In"
              screenOptions={{
                gestureEnabled: true,
                gestureDirection: "horizontal",
                ...TransitionPresets.SlideFromRightIOS
              }}
            >
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen 
                name="Sign In" 
                component={SignInScreen} 
                options={{
                  headerShown: false,
                }}
              />
            </Stack.Navigator>
          </NavigationNativeContainer>
        }
      </>);
  }
}